package com.chhen.homework;

import android.media.Image;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.chhen.homework.fragment_person.all_fragment_bodies.BodyEightFragment;
import com.chhen.homework.fragment_person.all_fragment_bodies.BodyElevenFragment;
import com.chhen.homework.fragment_person.all_fragment_bodies.BodyFiveFragment;
import com.chhen.homework.fragment_person.all_fragment_bodies.BodyFourFragment;
import com.chhen.homework.fragment_person.all_fragment_bodies.BodyNineFragment;
import com.chhen.homework.fragment_person.all_fragment_bodies.BodyOneFragment;
import com.chhen.homework.fragment_person.all_fragment_bodies.BodySevenFragment;
import com.chhen.homework.fragment_person.all_fragment_bodies.BodySixFragment;
import com.chhen.homework.fragment_person.all_fragment_bodies.BodyTenFragment;
import com.chhen.homework.fragment_person.all_fragment_bodies.BodyThreeFragment;
import com.chhen.homework.fragment_person.all_fragment_bodies.BodyTwelveFragment;
import com.chhen.homework.fragment_person.all_fragment_bodies.BodyTwoFragment;
import com.chhen.homework.fragment_person.all_fragment_heads.HeadEightFragment;
import com.chhen.homework.fragment_person.all_fragment_heads.HeadElevenFragment;
import com.chhen.homework.fragment_person.all_fragment_heads.HeadFiveFragment;
import com.chhen.homework.fragment_person.all_fragment_heads.HeadFourFragment;
import com.chhen.homework.fragment_person.all_fragment_heads.HeadNineFragment;
import com.chhen.homework.fragment_person.all_fragment_heads.HeadOneFragment;
import com.chhen.homework.fragment_person.all_fragment_heads.HeadSevenFragment;
import com.chhen.homework.fragment_person.all_fragment_heads.HeadSixFragment;
import com.chhen.homework.fragment_person.all_fragment_heads.HeadTenFragment;
import com.chhen.homework.fragment_person.all_fragment_heads.HeadThreeFragment;
import com.chhen.homework.fragment_person.all_fragment_heads.HeadTwelveFragment;
import com.chhen.homework.fragment_person.all_fragment_heads.HeadTwoFragment;
import com.chhen.homework.fragment_person.all_fragment_legs.LegsEightFragment;
import com.chhen.homework.fragment_person.all_fragment_legs.LegsElevenFragment;
import com.chhen.homework.fragment_person.all_fragment_legs.LegsFiveFragment;
import com.chhen.homework.fragment_person.all_fragment_legs.LegsFourFragment;
import com.chhen.homework.fragment_person.all_fragment_legs.LegsNineFragment;
import com.chhen.homework.fragment_person.all_fragment_legs.LegsOneFragment;
import com.chhen.homework.fragment_person.all_fragment_legs.LegsSevenFragment;
import com.chhen.homework.fragment_person.all_fragment_legs.LegsSixFragment;
import com.chhen.homework.fragment_person.all_fragment_legs.LegsTenFragment;
import com.chhen.homework.fragment_person.all_fragment_legs.LegsThreeFragment;
import com.chhen.homework.fragment_person.all_fragment_legs.LegsTwelveFragment;
import com.chhen.homework.fragment_person.all_fragment_legs.LegsTwoFragment;

public class DisplayPersonActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction transaction;
    Fragment fragment;

    FragmentContainerView head;
    FragmentContainerView body;
    FragmentContainerView legs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_person);

        // init event on click image view
        head = findViewById(R.id.head_fragment);
        body = findViewById(R.id.body_fragment);
        legs = findViewById(R.id.legs_fragment);

//        fragmentManager.addOnBackStackChangedListener(() -> Toast.makeText(getApplicationContext(), "Message", Toast.LENGTH_SHORT).show());

        // on click to change image head
        head.setOnClickListener(v -> {
            onReplaceHead();
        });

        // on click to change image body
        body.setOnClickListener(v -> {
            onReplaceBody();
        });

        // on click to change image legs
        legs.setOnClickListener(v -> {
            onReplaceLegs();
        });


    }

    // on click to change image head
    private void onReplaceHead() {
        fragment = fragmentManager.findFragmentById(R.id.head_fragment);
        if (fragment instanceof HeadOneFragment) {
            fragment = new HeadTwoFragment();

        } else if (fragment instanceof HeadTwoFragment) {
            fragment = new HeadThreeFragment();

        } else if (fragment instanceof HeadThreeFragment) {
            fragment = new HeadFourFragment();

        } else if (fragment instanceof HeadFourFragment) {
            fragment = new HeadFiveFragment();

        } else if (fragment instanceof HeadFiveFragment) {
            fragment = new HeadSixFragment();

        } else if (fragment instanceof HeadSixFragment) {
            fragment = new HeadSevenFragment();

        } else if (fragment instanceof HeadSevenFragment) {
            fragment = new HeadEightFragment();

        } else if (fragment instanceof HeadEightFragment) {
            fragment = new HeadNineFragment();

        } else if (fragment instanceof HeadNineFragment) {
            fragment = new HeadTenFragment();

        } else if (fragment instanceof HeadTenFragment) {
            fragment = new HeadElevenFragment();

        } else if (fragment instanceof HeadElevenFragment) {
            fragment = new HeadTwelveFragment();

        } else if (fragment instanceof HeadTwelveFragment) {
            fragment = new HeadOneFragment();
        } else {
            fragment = new HeadOneFragment();
        }
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.head_fragment, fragment, "headFragment")
                .addToBackStack(null)
                .commit();
    }

    // on click to change image body
    private void onReplaceBody() {
        fragment = fragmentManager.findFragmentById(R.id.body_fragment);
        if (fragment instanceof BodyOneFragment) {
            fragment = new BodyTwoFragment();

        } else if (fragment instanceof BodyTwoFragment) {
            fragment = new BodyThreeFragment();

        } else if (fragment instanceof BodyThreeFragment) {
            fragment = new BodyFourFragment();

        } else if (fragment instanceof BodyFourFragment) {
            fragment = new BodyFiveFragment();

        } else if (fragment instanceof BodyFiveFragment) {
            fragment = new BodySixFragment();

        } else if (fragment instanceof BodySixFragment) {
            fragment = new BodySevenFragment();

        } else if (fragment instanceof BodySevenFragment) {
            fragment = new BodyEightFragment();

        } else if (fragment instanceof BodyEightFragment) {
            fragment = new BodyNineFragment();

        } else if (fragment instanceof BodyNineFragment) {
            fragment = new BodyTenFragment();

        } else if (fragment instanceof BodyTenFragment) {
            fragment = new BodyElevenFragment();

        } else if (fragment instanceof BodyElevenFragment) {
            fragment = new BodyTwelveFragment();

        } else if (fragment instanceof BodyTwelveFragment) {
            fragment = new BodyOneFragment();
        } else {
            fragment = new BodyOneFragment();
        }

        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.body_fragment, fragment, "bodyFragment")
                .addToBackStack(null)
                .commit();
    }

    // on click to change image legs
    private void onReplaceLegs() {
        fragment = fragmentManager.findFragmentById(R.id.legs_fragment);
        if (fragment instanceof LegsOneFragment) {
            fragment = new LegsTwoFragment();

        } else if (fragment instanceof LegsTwoFragment) {
            fragment = new LegsThreeFragment();

        } else if (fragment instanceof LegsThreeFragment) {
            fragment = new LegsFourFragment();

        } else if (fragment instanceof LegsFourFragment) {
            fragment = new LegsFiveFragment();

        } else if (fragment instanceof LegsFiveFragment) {
            fragment = new LegsSixFragment();

        } else if (fragment instanceof LegsSixFragment) {
            fragment = new LegsSevenFragment();

        } else if (fragment instanceof LegsSevenFragment) {
            fragment = new LegsEightFragment();

        } else if (fragment instanceof LegsEightFragment) {
            fragment = new LegsNineFragment();

        } else if (fragment instanceof LegsNineFragment) {
            fragment = new LegsTenFragment();

        } else if (fragment instanceof LegsTenFragment) {
            fragment = new LegsElevenFragment();

        } else if (fragment instanceof LegsElevenFragment) {
            fragment = new LegsTwelveFragment();

        } else if (fragment instanceof LegsTwelveFragment) {
            fragment = new LegsOneFragment();
        } else {
            fragment = new LegsOneFragment();
        }

        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.legs_fragment, fragment, "LegsFragment")
                .addToBackStack(null)
                .commit();
    }

    // on click back to change all image to default
    @Override
    public void onBackPressed() {
        Fragment fragmentHead = fragmentManager.findFragmentById(R.id.head_fragment);
        Fragment fragmentBody = fragmentManager.findFragmentById(R.id.body_fragment);
        Fragment fragmentLegs = fragmentManager.findFragmentById(R.id.legs_fragment);

        if (fragmentHead != null) {
            transaction = fragmentManager.beginTransaction();
            transaction.remove(fragmentHead);
            transaction.commit();
        } else {
            super.onBackPressed();
        }

        if (fragmentBody != null) {
            transaction = fragmentManager.beginTransaction();
            transaction.remove(fragmentBody);
            transaction.commit();
        } else {
            super.onBackPressed();
        }

        if (fragmentLegs != null) {
            transaction = fragmentManager.beginTransaction();
            transaction.remove(fragmentLegs);
            transaction.commit();
        } else {
            super.onBackPressed();
        }
    }

}