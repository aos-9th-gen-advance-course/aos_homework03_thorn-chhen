package com.chhen.homework;

public class Person {

    private int id;
    private int image;

    public Person() {

    }

    public Person(int id, int image) {
        this.id = id;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", image=" + image +
                '}';
    }
}
