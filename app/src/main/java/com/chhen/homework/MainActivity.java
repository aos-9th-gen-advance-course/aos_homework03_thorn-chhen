package com.chhen.homework;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private AppCompatButton mButtonDisplay;
    Intent intent;

    private RecyclerView mPersonRecyclerView;
    private GridLayoutManager mLayoutManager;
    private PersonAdapter mPersonAdapter;
    private ArrayList<Person> mDataSet;

    ImageView mImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mImage = findViewById(R.id.image_view);

        // init button
        mButtonDisplay = findViewById(R.id.btn_display_person);
        mButtonDisplay.setOnClickListener(v -> {
            onButtonDisplayPerson();
        });

        // init recycler view
        mPersonRecyclerView = findViewById(R.id.recycler_view_person);

        // init layout manager
        int numberOfColumns = 2;
        mLayoutManager = new GridLayoutManager(this, numberOfColumns);

        // set layout manager
        mPersonRecyclerView.setLayoutManager(mLayoutManager);

        // create our dataset
        mDataSet = new ArrayList<>();

        // heads
        mDataSet.add(new Person(1, R.drawable.head1));
        mDataSet.add(new Person(2, R.drawable.head2));
        mDataSet.add(new Person(3, R.drawable.head3));
        mDataSet.add(new Person(4, R.drawable.head4));
        mDataSet.add(new Person(5, R.drawable.head5));
        mDataSet.add(new Person(6, R.drawable.head6));
        mDataSet.add(new Person(7, R.drawable.head7));
        mDataSet.add(new Person(8, R.drawable.head8));
        mDataSet.add(new Person(9, R.drawable.head9));
        mDataSet.add(new Person(10, R.drawable.head10));
        mDataSet.add(new Person(11, R.drawable.head11));
        mDataSet.add(new Person(12, R.drawable.head12));

        // bodies
        mDataSet.add(new Person(13, R.drawable.body1));
        mDataSet.add(new Person(14, R.drawable.body2));
        mDataSet.add(new Person(15, R.drawable.body3));
        mDataSet.add(new Person(16, R.drawable.body4));
        mDataSet.add(new Person(17, R.drawable.body5));
        mDataSet.add(new Person(18, R.drawable.body6));
        mDataSet.add(new Person(19, R.drawable.body7));
        mDataSet.add(new Person(20, R.drawable.body8));
        mDataSet.add(new Person(21, R.drawable.body9));
        mDataSet.add(new Person(22, R.drawable.body10));
        mDataSet.add(new Person(23, R.drawable.body11));
        mDataSet.add(new Person(24, R.drawable.body12));

        // legs
        mDataSet.add(new Person(25, R.drawable.legs1));
        mDataSet.add(new Person(26, R.drawable.legs2));
        mDataSet.add(new Person(27, R.drawable.legs3));
        mDataSet.add(new Person(28, R.drawable.legs4));
        mDataSet.add(new Person(29, R.drawable.legs5));
        mDataSet.add(new Person(30, R.drawable.legs6));
        mDataSet.add(new Person(31, R.drawable.legs7));
        mDataSet.add(new Person(32, R.drawable.legs8));
        mDataSet.add(new Person(33, R.drawable.legs9));
        mDataSet.add(new Person(34, R.drawable.legs10));
        mDataSet.add(new Person(35, R.drawable.legs11));
        mDataSet.add(new Person(36, R.drawable.legs12));

        // init adapter
        mPersonAdapter = new PersonAdapter(this, mDataSet);

        // set adapter
        mPersonRecyclerView.setAdapter(mPersonAdapter);


    }

    private void onButtonDisplayPerson() {
        intent = new Intent(getApplicationContext(), DisplayPersonActivity.class);
        startActivity(intent);
    }
}