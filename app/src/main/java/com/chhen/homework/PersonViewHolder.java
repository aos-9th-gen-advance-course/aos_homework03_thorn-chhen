package com.chhen.homework;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class PersonViewHolder extends RecyclerView.ViewHolder {

    ImageView mImageView;
    List<Person> getItemsSelect = new ArrayList<>();
    Person person = new Person();

    public PersonViewHolder(@NonNull View itemView) {
        super(itemView);
        mImageView = itemView.findViewById(R.id.image_view);


        mImageView.setOnClickListener(v -> {
            Log.d("TAG", "PersonViewHolder: " + getAdapterPosition());
            person.setImage(getAdapterPosition());
        });

    }

}
